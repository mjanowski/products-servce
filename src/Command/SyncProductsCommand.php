<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:sync-products',
    description: 'Synchronize products using all available data sources',
)]
class SyncProductsCommand extends Command
{
    public function __construct(private readonly array $importers)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('Sync process started...');

        foreach ($this->importers as $importer) {
            $importer->addBatchToQueue();
        }

        $io->success('Importers added to queue');

        return Command::SUCCESS;
    }
}
