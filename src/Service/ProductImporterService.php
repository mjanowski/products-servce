<?php

namespace App\Service;

use App\DataSource\DataSourceInterface;
use App\Entity\Product;
use App\Message\CreateProductsBatchMessage;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\MessageBusInterface;

class ProductImporterService
{
    private int $offset = 0;

    public function __construct(
        private readonly DataSourceInterface $dataSource,
        private readonly ProductRepository $productRepository,
        private readonly MessageBusInterface $messageBus
    ){}

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function sync(): void
    {
        $items = $this->dataSource->setOffset($this->offset)->getItems();
        if (count($items) == $this->dataSource->getBatchSize()) {
            $this->addBatchToQueue($this->offset + $this->dataSource->getBatchSize());
        }
        $this->updateProducts($items);
    }

    public function addBatchToQueue(int $offset = 0): void
    {
        $this->messageBus->dispatch(new CreateProductsBatchMessage(
            $this->dataSource->getSourceName(),
            $offset
        ));
    }

    private function updateProducts(array $items): void
    {
        foreach ($items as $item) {
            $product = $this->productRepository->findOneBy(['externalId' => $item['id'], 'source' => $this->dataSource->getSourceName()]);
            if (!$product || $product->getUpdatedAt() != (new \DateTimeImmutable($item['updatedAt']))) {
                $product = $product ?: new Product();
                $product->setExternalId($item['id'])
                    ->setTitle($item['title'])
                    ->setPrice($item['price'])
                    ->setImages(serialize($item['images']))
                    ->setCreationAt(new \DateTimeImmutable($item['creationAt']))
                    ->setUpdatedAt(new \DateTimeImmutable($item['updatedAt']))
                    ->setSource($this->dataSource->getSourceName());

                $this->productRepository->save($product, true);
            }

        }
    }
}
