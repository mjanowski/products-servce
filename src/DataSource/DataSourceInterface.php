<?php

namespace App\DataSource;

interface DataSourceInterface
{
    public function getSourceName(): string;
    public function setOffset(int $offset): self;
    public function getBatchSize(): int;
    public function getItems(): array;
}
