<?php

namespace App\DataSource;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FileDataSource implements DataSourceInterface
{
    public const SOURCE_NAME = 'importer.file';
    private int $offset = 0;
    private int $batchSize;
    private string $filename;

    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
        $this->filename = $this->parameterBag->get('importer')['file']['filename'];
        $this->batchSize = $this->parameterBag->get('importer')['file']['batchSize'];
    }
    public function getSourceName(): string
    {
        return self::SOURCE_NAME;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function getBatchSize(): int
    {
        return $this->batchSize;
    }

    public function getItems(): array
    {
        $row = 0;
        $headers = [];
        $items = [];
        if (($handle = fopen($this->filename, "r")) !== false) {
            while (($data = fgetcsv($handle, 10000, ",")) !== false) {
                if ($row > ($this->offset + $this->batchSize)) {
                    break;
                }
                if ($row == 0) {
                    $headers = $data;
                }
                if ($row > $this->offset) {
                    $num = count($data);
                    $item = [];
                    for ($c=0; $c < $num; $c++) {
                        $item[$headers[$c]] = $data[$c];
                    }
                    $items[] = $item;
                }

                $row++;
            }
            fclose($handle);
        }
        return $items;
    }
}
