<?php

namespace App\DataSource;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiDataSource implements DataSourceInterface
{
    public const SOURCE_NAME = 'importer.api';
    private int $offset = 0;
    private int $batchSize;

    public function getSourceName(): string
    {
        return self::SOURCE_NAME;
    }

    public function __construct(private HttpClientInterface $httpClient, private readonly ParameterBagInterface $parameterBag)
    {
        $this->httpClient = $httpClient->withOptions([
            'base_uri' => $this->parameterBag->get('importer')['api']['url']
        ]);
        $this->batchSize = $this->parameterBag->get('importer')['api']['batchSize'];
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function getBatchSize(): int
    {
        return $this->batchSize;
    }

    private function sendRequest(string $method, string $url, array $data = [])
    {
        $response = $this->httpClient->request($method, $url, $data);
        return ($response->getContent() ? json_decode($response->getContent(), true) : []);
    }

    public function getItems(): array
    {
        return $this->sendRequest('GET', '', ['query' => ['offset' => $this->offset, 'limit' => $this->batchSize]]);
    }




}
