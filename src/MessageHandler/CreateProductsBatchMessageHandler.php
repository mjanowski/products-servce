<?php

namespace App\MessageHandler;

use App\Message\CreateProductMessage;
use App\Message\CreateProductsBatchMessage;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class CreateProductsBatchMessageHandler
{
    public function __construct(private readonly KernelInterface $kernel)
    {}

    public function __invoke(CreateProductsBatchMessage $createProductsBatchMessage): void
    {
        $importer = $this->kernel->getContainer()->get($createProductsBatchMessage->getImporterName());
        $importer->setOffset($createProductsBatchMessage->getOffset())->sync();
    }
}
