<?php

namespace App\Message;

class CreateProductsBatchMessage
{
    public function __construct(
        private readonly string $importerName,
        private readonly string $offset
    )
    {}

    public function getImporterName(): string
    {
        return $this->importerName;
    }

    public function getOffset(): string
    {
        return $this->offset;
    }
}
