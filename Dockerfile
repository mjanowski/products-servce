FROM php:8.1-apache-buster
RUN apt-get -qq update && apt-get -qq -y install  \
  automake \
  cmake \
  g++ \
  git \
  libicu-dev \
  libmagickwand-dev \
  libpng-dev \
  librabbitmq-dev \
  libreadline-dev \
  libzip-dev \
  zlib1g-dev \
  pkg-config \
  ssh-client \
  && docker-php-ext-install \
  bcmath \
  gd \
  intl \
  opcache \
  pdo_mysql \
  sockets \
  zip

RUN pecl install amqp imagick xdebug igbinary \
  && docker-php-ext-enable amqp imagick xdebug igbinary pdo_mysql

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
