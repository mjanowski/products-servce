# Products service

Synchronize products data based on different types of data source
## Prerequirements
To build & run this project you have to make sure that you have `Docker` and `Docker Compose` installed.

## Build & run containers

Build & run containers `docker-compose up`

Example output will be available at `http://localhost:80`

RabbitMQ management: `http://localhost:15672/`
User / pass: `rabbit:rabbit`

## Usage
Run synchronization - add importers batch to queue:

`docker-compose exec app bin/console app:sync-products`

Run the worker to consume messages:

`docker-compose exec app bin/console messenger:consume`

## Settings and extensions

Settings the data source file and batch size.
All the settings you can set in `config/services.yaml` file
```
parameters:
    importer:
        api:
            url: 'https://api.escuelajs.co/api/v1/products'
            batchSize: 50
        file:
            filename: '/var/www/html/products.csv'
            batchSize: 5
```

Adding new data source:

You can define a new data source by creating a new class which implements `DataSourceInterface`. 
The necessary settings should be defined in services.yaml:

```
    importer.api:
        class: App\Service\ProductImporterService
        public: true
        arguments:
            $dataSource: '@App\DataSource\ApiDataSource'
    importer.file:
        class: App\Service\ProductImporterService
        public: true
        arguments:
            $dataSource: '@App\DataSource\FileDataSource'
            
            
            
    App\Command\SyncProductsCommand:
        class: App\Command\SyncProductsCommand
        autowire: false
        arguments:
            $importers: ['@importer.file', '@importer.api']
```

